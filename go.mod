module gitlab.bcowtech.de/bcow-go/elasticsearch-esresponse/v7

go 1.14

require (
	github.com/elastic/go-elasticsearch v0.0.0
	github.com/elastic/go-elasticsearch/v7 v7.10.0
	github.com/pkg/errors v0.9.1
	gitlab.bcowtech.de/bcow-go/elasticsearch-esresponse v0.0.0-20210123170550-4f3a1b9f5277
)
